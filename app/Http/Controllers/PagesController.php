<?php

namespace App\Http\Controllers;

use App\Post;

class PagesController extends Controller{
	public function getIndex(){

		$posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
		return view('pages.welcome')->withPosts($posts);
		//return view('pages.welcome');

	}

	public function getAbout(){

		$first = "Jahirul";
		$last = "Islam";
		$fullname = $first." ".$last;
		$email = "jir71bd@gamil.com";
		$data = [];
		$data['fullname']=$fullname;
		$data['email'] = $email;
		return view('pages.about')->withData($data);
	}

	public function getContact(){
		return view('pages.contact');
	}

	public function getConnect(){
		return view('pages.connect');
	}

	public function getSupplier(){
		return view('pages.supplier');
	}
}

?>