@extends('main')



@section('title'," | View Post")


@section('content')
	

	<div class="row">
		<div class="col-md-8">
			<h2>{{ $supplier->company_name }}</h2>
			<h2>{{ $supplier->first_name }}</h2>
			<h3>{{ $supplier->email }}</h3>
			<h4>{{ $supplier->mobile_no }}</h4>
			
			<p class="lead">{!! $supplier->address !!}</p>
			<p class="lead">{!! $supplier->comments !!}</p>
			<p class="lead">{!! $supplier->account !!}</p>

			<hr>
			
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Create At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($supplier->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>last Update:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($supplier->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					
				</dl>

				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('suppliers.edit', 'Edit', array($supplier->id), array('class' => 'btn btn-primary btn-block')) !!}
						<!--<a href="#" class="btn btn-primary btn-block">Edit</a>-->
					</div>
					<div class="col-sm-6">
						{!! Form::open(['route' => ['suppliers.destroy', $supplier->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}

						<!--<a href="#" class="btn btn-danger btn-block">Delete</a>-->
					</div>
				</div>


			</div>
		</div>
	</div>


@endsection