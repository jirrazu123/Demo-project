@extends('main')

@section('title', '| Edit Blog Post')


@section('content')

	<div class="row">
		{!! Form::model($supplier, ['route' => ['suppliers.update', $supplier->id], 'method'=>'PUT']) !!}
		<div class="col-md-8">

			{{ Form::label('company_name','Company Name:')}}
            {{ Form::text('company_name', null, ["class" => 'form-control input-lg'])}}

            {{ Form::label('first_name','First Name:')}}
            {{ Form::text('first_name', null, ["class" => 'form-control input-lg'])}}

            {{ Form::label('last_name','Last Name:')}}
            {{ Form::text('last_name', null, ["class" => 'form-control input-lg'])}}

            {{ Form::label('email','Email:')}}
            {{ Form::text('email', null, ["class" => 'form-control input-lg'])}}

            {{ Form::label('mobile_no','Mobile No:')}}
            {{ Form::text('mobile_no', null, ["class" => 'form-control input-lg'])}}

            {{ Form::label('address','Address:',['class' => 'form-spacing-top'])}}
            {{ Form::textarea('address', null, ['class' => 'form-control'])}}

            {{ Form::label('comments','Comments:',['class' => 'form-spacing-top'])}}
            {{ Form::textarea('comments', null, ['class' => 'form-control'])}}

            {{ Form::label('account','Account:')}}
            {{ Form::text('account', null, array('class'=>'form-control'))}}

            {{ Form::submit('Create Supplier', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}

		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Created At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($supplier->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Last Updated:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($supplier->updated_at)) }}</dd>
				</dl>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('suppliers.show', 'Cancel', array($supplier->id), array('class' => 'btn btn-danger btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{{ Form::submit('Save Changes', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>

			</div>
		</div>
		{!! Form::close() !!}
	</div>	<!-- end of .row (form) -->

@stop
