@extends('main')

@section('title', '| All Posts')

@section('content')

	<div class="row">
		<div class="col-md-9">
			<h1>All Supplier</h1>
		</div>

		<div class="col-md-3">
			<a href="{{route('suppliers.create')}}" class="btn btn-lg btn-block btn-info btn-h1-spacing">Create New Supplier</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div> <!-- end of .row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Company Name</th>
					<th>Supplier Name</th>
					<th>E-mail</th>
					<th>Mobile No</th>
					<th>Address</th>
					<th>Comments</th>
					<th>Account</th>
					<th>Created At</th>
					<th></th>
				</thead>

				<tbody>
					@foreach ($supplier as $supplier)
						
						<tr>
							<th>{{ $supplier->id }}</th>
							<td>{{ $supplier->company_name }}</td>
							<td>{{ $supplier->first_name }}</td>
							<td>{{ $supplier->email }}</td>
							<td>{{ $supplier->mobile_no }}</td>
							<td>{{ $supplier->address }}</td>
							<td>{{ $supplier->comments }}</td>
							<td>{{ $supplier->account }}</td>
							<td><a href="{{ route('suppliers.show', $supplier->id) }}" class="btn btn-default btn-sm">View</a> <a href="{{ route('suppliers.show', $supplier->id) }}" class="btn btn-default btn-sm">Edit</a></td>
						</tr>

					@endforeach
				</tbody>
			</table>

		</div>
	</div>

@stop