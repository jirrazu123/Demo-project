@extends('main')

@section('title', '| Contact')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Contact me by Email</h3>
            <hr>
            <form>
                <div class="form-group">
                    <label name="email">Email: </label>
                    <input type="email" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label name="subject">Subject: </label>
                    <input type="subject" name="subject" class="form-control">
                </div>

                <div class="form-group">
                    <label name="massage">Massage: </label>
                    <textarea id="massage" name="massage" class="form-control">Type your massage here...</textarea>
                </div>

                <input type="submit" name="" value="Send Massage" class="btn btn-success">
            </form>
        </div>
    </div>
</div><!--end of container-->
@endsection