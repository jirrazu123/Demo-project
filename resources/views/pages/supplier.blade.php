@extends('main')

@section('title', '| Supplier')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3>Create   Supplier</h3>
            <hr>
            <form>
                <div class="form-group">
                    <label name="comapny_name">Comapny Name:</label>
                    <input type="comapny_name" name="comapny_name" class="form-control">
                </div>

                <div class="form-group">
                    <label name="first_name">First Name:</label>
                    <input type="first_name" name="first_name" class="form-control">
                </div>


                <div class="form-group">
                    <label name="last_name">Last Name:</label>
                    <input type="last_name" name="last_name" class="form-control">
                </div>

                <div class="form-group">
                    <label name="email">E-mail:</label>
                    <input type="email" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <label name="mobile_no">Mobile No: </label>
                    <input type="mobile_no" name="mobile_no" class="form-control">
                </div>

                <div class="form-group">
                    <label name="address">Address: </label>
                    <textarea id="address" name="address" class="form-control">Type your adress here...</textarea>
                </div>

                <div class="form-group">
                    <label name="comment">Comments: </label>
                    <textarea id="comment" name="comment" class="form-control">Type your comment here...</textarea>
                </div>

                <div class="form-group">
                    <label name="account">Account: </label>
                    <input type="account" name="account" class="form-control">
                </div>

                <input type="submit" name="" value="Send Massage" class="btn btn-success">
            </form>
        </div>
    </div>
</div><!--end of container-->
@endsection